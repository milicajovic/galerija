<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<style>
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 319px;
  height:280px;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 6px;
  float: left;
  width: 24.99999%;
 
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 50%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
  }
}

 .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

body{
	color:#00ffff;
	background-color:black;
}

.h2{
	color:#00ffff;
}


</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${!empty poruka}">
			<div id="sadrzaj">
			<div id="poruke">
				<div>${poruka }</div>
			</div>
			</div>
	</c:if>
	<c:if test="${!empty message }">
         <h2> ${message }</h2>
                
    </c:if> 
	   
 		
	     	<c:forEach items="${slike }" var="s" >
				
					<div class="responsive">
					 <div class="gallery">
					<c:if test="${!empty s.image }">
					
					<a target="_blank" href="/Galerija/Slike/get-image/${s.idSlika }">
					<img  src="/Galerija/Slike/get-image/${s.idSlika } " width="600" height="400">
					</a>
					</c:if><br>
					<div class="desc">
					Naziv:<i>${s.naziv }</i><br>
					Autor:<i><a href="/Galerija/Autor/prikazAutora?idAutor=${s.autor.idAutor}">${s.autor.imeAutora }</a></i><br>
					Pravac:<i>${s.pravac }</i><br>
					Godina:<i>${s.vremeNastanka}</i><br>
					<a href="/Galerija/Slike/pregledUtisaka?idSlika=${s.idSlika }">Pogledaj utiske</a>
					<form action="/Galerija/Slike/lajkuj" method="post"><input type="hidden" name="idSlika" value="${s.idSlika }"/><button class="btn">Svidja mi se</button></form>
					
					</div>
					</div>
					</div>
			</c:forEach>
			
			
			<c:if test="${empty slike }">
				Trenutno nema slika!
			</c:if>
		
	
 


</body>
</html>