<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
	.main {
  		margin-left: 290px; 
 		padding: 0px 10px;
 		margin-top: 0px;
 		
	}
	
	.main2{
		text-align: justify;
		
	}
	
	.center{
		width: 35%;
	}
	
	.center2{
		width: 70%;
	}
	
	.center3{
		width: 80%;
		margin-top:5px;
	}
	.center4{
		width: 100%;
		margin-top:1px;
	}
	.center5{
		
		
		margin-left:300px;
	}
	.center6{
		margin-left:350px;
	}
	
	.center7{
		margin-left:76px;
	}
	
	.field{
  		border-color: #DCDCDC;
  		
  		
	}
	
	.button{
		 background-color: #262626;
  		 border: none;
  		 color: white;
 		 padding: 15px 32px;
 		 text-align: center;
 		 text-decoration: none;
  		 display: inline-block;
 		 font-size: 14px;
 		 cursor: pointer;
 		 margin-left:200px;
	}
</style>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    

    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
    
    
    
    
  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<title>GALLERY</title>
</head>
<body>

	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
			<img src="${pageContext.request.contextPath}/img/unnamed.png" class="center2"/><br><br><br><br><br>
			<nav id="colorlib-main-menu" role="navigation">
			
				<ul>
					<li class="colorlib-active"><a href="/Galerija/Pocetna.jsp">Home</a></li>
					<li><a href="/Galerija/Gallery.jsp">Gallery</a></li>
					<li><a href="/Galerija/Forum.jsp">Forum</a></li>
					<li><a href="/Galerija/Tutorijal.jsp">Tutorial</a></li>
					<li><a href="/Galerija/LogovanjeRegistracija/odjava">Odjavi se</a></li>
					
					
				</ul>
			</nav>

			<div class="colorlib-footer">
				<h3>Follow Us Here!</h3>
				<div class="d-flex justify-content-center">
					<ul class="d-flex align-items-center">
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/fb.png" class="center3"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/insta.png" class="center4"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="https://twitter.com/"><img src="${pageContext.request.contextPath}/img/tw.png" class="center4"/></a></li>
						
					</ul>
				</div>
			</div>
		</aside> <!-- END COLORLIB-ASIDE -->
		<div class="main">
			<img src="${pageContext.request.contextPath}/img/gallery.png" class="center"/>
		</div>
		
		
	     <div class="center5">   
	     ${user.username }
	     <a href="/Galerija/Slike/nadjiNajveci" >Korisnik sa najvise dodatih slika je: </a>
	   		
			<c:if test="${!empty najveci }">${najveci }</c:if><br>
			
		<a href="/Galerija/Slike/najviseLajkova" >Slika sa najvise lajkova u poslednjih mesec dana: </a>
	   		
			<c:if test = "${!empty slikaSaNajviseLajkova }">
	   		     ${slikaSaNajviseLajkova.naziv }
	   		  </c:if>
		
	     
	     <form action="/Galerija/Slike/sveSlike" method="get">
	     	<c:forEach items="${slike }" var="s" >
				<fieldset class="field">
					<div class="center6">
					<c:if test="${!empty s.image }">
					<img height="260" width="250" src="/Galerija/Slike/get-image/${s.idSlika } "/>
					</c:if><br>
					<div class="main2">
					Naziv:<i>${s.naziv }</i><br>
					Autor:<i><a href="/Galerija/Autor/prikazAutora?idAutor=${s.autor.idAutor}">${s.autor.imeAutora }</a></i><br>
					Pravac:<i>${s.pravac }</i><br>
					Godina:<i>${s.vremeNastanka}</i><br>
					<div class="center7"><b><a href="/Galerija/Slike/utisci?idSlika=${s.idSlika }">Unesite utisak</a></b></div>
					<div class="center7"><b><a href="/Galerija/Slike/pregledUtisaka?idSlika=${s.idSlika }">Pogledaj utiske</a></b></div>
					</div>
					</div>
				</fieldset>	
			
			
			</c:forEach>
	     </form> 
	     
	        <a href="/Galerija/Admin/unosSlikeInit" class="button"><b>Dodaj zanimljivosti</b></a>
	        <a href="/Galerija/Admin/sveZanimljivosti" class="button"><b>Pregled zanimljivosti</b></a>
		
		
		</div>
		
		
		</div>
	
	
</body>
</html>