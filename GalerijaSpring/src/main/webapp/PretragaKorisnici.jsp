<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
	.main {
  		margin-left: 660px; 
 		padding: 0px 10px;
 		margin-top: 100px;
 		
	}
	
	.center{
		width: 35%;
	}
	
	.center2{
		width: 70%;
	}
	
	.center3{
		width: 80%;
		margin-top:5px;
	}
	.center4{
		width: 100%;
		margin-top:1px;
		margin-left:70px;
	}
	
	.center6{
		margin-left:25px;
	}
	
	.center5{
		margin-left:700px;
		margin-top:5px;
	}
	
	
	.field{
  		border-color: #DCDCDC;
  		
  		
	}
</style>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
     
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<title>Welcome</title>
</head>
<body>

 	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
			<img src="${pageContext.request.contextPath}/img/unnamed.png" class="center2"/><br><br><br><br><br>
			<nav id="colorlib-main-menu" role="navigation">
			
				<ul>
					<li class="colorlib-active"><a href="/Galerija/Pocetna.jsp">Home</a></li>
					<li><a href="/Galerija/Gallery.jsp">Gallery</a></li>
					<li><a href="/Galerija/Forum.jsp">Forum</a></li>
					<li><a href="/Galerija/Tutorijal.jsp">Tutorial</a></li>
					<li><a href="/Galerija/LogovanjeRegistracija/logout">Logout</a></li>
					
				</ul>
			</nav>

			
		</aside> <!-- END COLORLIB-ASIDE -->
<div class="center5">
	<form action="/Galerija/Autor/getPretragaKorisnik" method="get">
	<select name="idUser">
			<c:forEach items="${user}" var="u">
				<option value="${u.idUser} "> ${u.ime} ${u.prezime }</option>
			</c:forEach>
		</select>
		<input type="submit" value="Pretrazi">
	</form>	
</div>	


<c:if test="${!empty slike }">

<div class="center4" style="font-size:20px; text-align:center;"> 

<c:if test="${!empty poruka }">	
		<h3>${poruka }</h3>
	</c:if>		
		
			<c:forEach  items="${slike }" var="rez">
				<fieldset class="field">
					<c:if test="${!empty rez.image }">
					<img height="260" width="250" src="/Galerija/Slike/get-image/${rez.idSlika } "/>
					</c:if><br>
					${rez.naziv }<br>
					<a href="/Galerija/Autor/prikazAutora?idAutor=${rez.autor.idAutor}">${rez.autor.imeAutora }</a><br>
					${rez.pravac }<br>
					${rez.vremeNastanka}<br>
				</fieldset>	
			
		
				
			</c:forEach>
			
	
	</div>
	
</c:if>	
	
</body>
</html>