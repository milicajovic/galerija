<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
	table {
  border-collapse: collapse;
  width:50%;
  table-layout:fixed;
  margin-left:450px;
  margin-top:10px;
}

td{
 width:50%;

}

th, td {
  text-align: center;
  padding: 8px;
  
}

tr:nth-child(even) {background-color: black;}
}
	
	
	
	
	

	
</style>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    

    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
    
    
    
    
  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<title>Welcome</title>
</head>
<body>

	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
			<img src="${pageContext.request.contextPath}/img/unnamed.png" class="center2"/><br><br><br><br><br>
			<nav id="colorlib-main-menu" role="navigation">
			
				<ul>
					<li class="colorlib-active"><a href="/Galerija/Pocetna.jsp">Home</a></li>
					<li><a href="/Galerija/Gallery.jsp">Gallery</a></li>
					<li><a href="/Galerija/Forum.jsp">Forum</a></li>
					<li><a href="/Galerija/Tutorijal.jsp">Tutorial</a></li>
					<li><a href="/Galerija/LogovanjeRegistracija/odjava">Odjavi se</a></li>
					
				</ul>
			</nav>

			
		</aside> <!-- END COLORLIB-ASIDE -->

		
		<table >
			
			
			
			<c:forEach var="z" items="${zanimljivosti }">
				<tr>
					<td>${z.tekst }</td>
					<td><c:if test="${!empty z.slika }">
						<img height="160" width="150" src="/Galerija/Admin/get-image/${z.idZanimljivosti } "/>
					</c:if><br></td>
				</tr>
			</c:forEach>
		</table>
		

	

			
		
			
				
					
				
					
				
			
	
	

	
</body>
</html>