<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
	.main {
  		margin-left: 200px; 
 		padding: 0px 10px;
 		margin-top: 0px;
 		
	}
	
	.main2 {
 
  padding: 0px 10px;
  margin-top: 10px;
  text-align: justify;
  text-justify: inter-word;
}
	
	table {
  border-collapse: collapse;
  width: 50%;
  margin-left:450px;
	}	
	
	.center{
		width: 50%;
		margin-left: 30px;
	}
	
	.center2{
		width: 70%;
	}
	
	.center3{
		width: 80%;
		margin-top:5px;
	}
	.center4{
		width: 100%;
		margin-top:1px;
	}
	
	.center5 {
  display: block;
  margin-left: 0%;
  margin-right: auto;
  width: 100%;
	}
	
	tr:nth-child(even) {background-color: #ffb3b3;}
	}
	
	.center6{
		margin-left:5%;
	}

</style>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    

    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
    
    
    
    
  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<title>Welcome</title>
</head>
<body>

	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
			<img src="${pageContext.request.contextPath}/img/unnamed.png" class="center2"/><br><br><br><br><br>
			<nav id="colorlib-main-menu" role="navigation">
			
				<ul>
					<li class="colorlib-active"><a href="/Galerija/Pocetna.jsp">Home</a></li>
					<li><a href="/Galerija/Gallery.jsp">Gallery</a></li>
					<li><a href="/Galerija/Forum.jsp">Forum</a></li>
					<li><a href="/Galerija/Tutorijal.jsp">Tutorial</a></li>
					<li><a href="/Galerija/LogovanjeRegistracija/odjava">Odjavi se</a></li>
					
				</ul>
			</nav>

			<div class="colorlib-footer">
				<h3>Follow Us Here!</h3>
				<div class="d-flex justify-content-center">
					<ul class="d-flex align-items-center">
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/fb.png" class="center3"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/insta.png" class="center4"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="https://twitter.com/"><img src="${pageContext.request.contextPath}/img/tw.png" class="center4"/></a></li>
						
					</ul>
				</div>
			</div>
		</aside> <!-- END COLORLIB-ASIDE -->
		<table border="7" bordercolor="#ffb3b3">
	<tr>
		<td>
		<div class="main" style="font-size:20px">
			<b>Šta su slikarske tehnike?</b><br/><br/>
		
				<img src="${pageContext.request.contextPath}/img/tehnike2.jpg" class="center"/>
		</div>
		<div class="main2">
				Slikarske tehnike su one u kojima je boja izražajno sredstvo.Nekada je to boja na podlozi a nekada su to materijali u raznim bojama od kojih se sastavlja kompozicija.
				U ranijim vremenima umetnici su  pigmente boja morali da traže u prirodi iz biljaka, minerala, mukotrpno ih izdvajali, sušili, drobili, mleli i mešali sa raznim vezivima.
				Danas su boje uglavnom sintetičke i već pripremljene za korišćenje.
		
			</div>
		</td>	
	</tr>
</table>
	
			
			
		
		
			
		<div class="center6"><h3>Tutorijali o tehnikama</h3></div>	
		<div class="center5" style="font-size:20px; text-align:center;">
		
		<table style="background: #ffe6e6">
			
			
			
			<c:forEach var="t" items="${tutorijali }">
				<tr>
					<td>${t.tutorijal}</td>
				</tr>
			</c:forEach>
		</table>
		
		
</div>
</body>
</html>