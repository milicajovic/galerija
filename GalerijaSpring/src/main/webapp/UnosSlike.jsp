<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html>
<head>
<style type="text/css">
	.main {
  		margin-left: 290px; 
 		padding: 0px 10px;
 		margin-top: 0px;
 		
	}
	
	.center{
		width: 35%;
	}
	
	.center2{
		width: 70%;
	}
	
	.center3{
		width: 80%;
		margin-top:5px;
	}
	.center4{
		width: 100%;
		margin-top:1px;
	}
	.center5{
		width:-10%;
		height: 500px;
		margin-left:280px;
	}
	
	.field{
  		border-color: #ff8080;
  		border-style: solid;
  		
	}
</style>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">

    

    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css">
    
    
    
    
  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

<title>GALLERY</title>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
			<img src="${pageContext.request.contextPath}/img/unnamed.png" class="center2"/><br><br><br><br><br>
			<nav id="colorlib-main-menu" role="navigation">
			
				<ul>
					<li class="colorlib-active"><a href="/Galerija/Pocetna.jsp">Home</a></li>
					<li><a href="/Galerija/Gallery.jsp">Gallery</a></li>
					<li><a href="/Galerija/Forum.jsp">Forum</a></li>
					<li><a href="/Galerija/Tutorijal.jsp">Tutorial</a></li>
					<li><a href="/Galerija/LogovanjeRegistracija/odjava">Odjavi se</a></li>
					
					
				</ul>
			</nav>

			<div class="colorlib-footer">
				<h3>Follow Us Here!</h3>
				<div class="d-flex justify-content-center">
					<ul class="d-flex align-items-center">
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/fb.png" class="center3"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="#"><img src="${pageContext.request.contextPath}/img/insta.png" class="center4"/></a></li>
						<li class="d-flex align-items-center jusitfy-content-center"><a href="https://twitter.com/"><img src="${pageContext.request.contextPath}/img/tw.png" class="center4"/></a></li>
						
					</ul>
				</div>
			</div>
		</aside> <!-- END COLORLIB-ASIDE -->
		<div class="main">
			<img src="${pageContext.request.contextPath}/img/gallery.png" class="center"/>
		</div>
		
			
	            
	
	
	
	<div class="center5">
	
	
			<fieldset class="field">
			<h2>Unos slike</h2>
			<form:form modelAttribute="slikaIm"
			
				action="/Galerija/Slike/sacuvajSliku" method="post"
				enctype="multipart/form-data">
				
				
				Autor<a href="/Galerija/UnosAutora.jsp"><b>(Dodajte novog autora)</b></a><br>
				<select name= "idAutor">
				<c:forEach items="${autori}" var="a">
				<option value="${a.idAutor}">
					${a.imeAutora}
				</option>
				</c:forEach>
				</select>
				<br>
				<br>
				Naziv<br>
				<form:input type="text" name="naziv" path="naziv" />
				<br>
				<br>
				Pravac <br>
				<form:input type="text" name="pravac" path="pravac" />
				<br>
				<br>
				Godina nastanka<br>
				<form:input type="text" name="vremeNastanka" path="vremeNastanka" />
				<br>
				<br>
				Slika <form:input type="file" path="image" />
				<br>
				<br>
				<input type="submit" value="Unesi" />
				
				<c:if test="${!empty poruka }">
				${poruka }
				</c:if>
				
			</form:form>
		  </fieldset>
	  </div>    
	    
	
	
	

</body>
</html>