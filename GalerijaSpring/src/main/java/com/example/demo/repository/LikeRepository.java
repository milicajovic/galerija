package com.example.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Lajk;
import model.Slika;
import model.User;

public interface LikeRepository extends JpaRepository<Lajk, Integer> {
	
	public Lajk findByUserAndSlika(User user, Slika idSlika);
	public List<Lajk> findByDatumGreaterThanEqual(String datumski);



}
