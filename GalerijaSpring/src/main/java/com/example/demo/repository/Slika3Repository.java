package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Slika;

public interface Slika3Repository extends JpaRepository<Slika, Integer> {
	
	public Optional<Slika> findByIdSlika(int najveci);

}
