package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import model.Forum;


public interface ForumRepository extends JpaRepository<Forum, Integer>{

	List<Forum> findAll();
}
