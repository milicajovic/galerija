package com.example.demo.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import model.Forum;
import model.Odgovor;
import model.User;

public interface OdgovorRepository extends JpaRepository<Odgovor, Integer>{

	List<Odgovor> findByForum(Forum f);
	List<User> findByUser(User u);
}
