package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Zanimljivosti;

public interface ZanimljivostiRepository extends JpaRepository<Zanimljivosti,Integer>{
	
	@Query("select z from Zanimljivosti z where z.tekst is not null")
	List <Zanimljivosti> sveZanimljivosti();

}
