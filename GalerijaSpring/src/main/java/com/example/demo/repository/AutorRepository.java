package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Autor;
import model.Slika;


public interface AutorRepository extends JpaRepository<Autor, Integer> {

	public Autor findByImeAutora(String imeAutora);

	//public Autor findByIdAutor(String izabranAutor);

	public Autor findByBiografija(Autor a);
	
	List<Autor> findBySlika(Slika s);

	
	
	
	
}
