package com.example.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import model.Role;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
