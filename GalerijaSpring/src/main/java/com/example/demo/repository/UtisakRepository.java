package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Slika;
import model.Utisak;

public interface UtisakRepository extends JpaRepository<Utisak, Integer> {
	
	List <Utisak> findBySlika(Slika s);
}
