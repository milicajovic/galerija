package com.example.demo.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import model.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {
	
	User findByUsername(String username);
	User findByUsernameAndPassword(String username, String password);
	
	@Query("select u from User u where u.idUser != 1")
	List <User> sviKorisnici();

	
}
