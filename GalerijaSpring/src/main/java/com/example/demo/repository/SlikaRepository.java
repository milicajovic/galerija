package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import model.Autor;
import model.Slika;
import model.User;

public interface SlikaRepository extends JpaRepository<Slika, Integer>,JpaSpecificationExecutor<Slika> {

	
	List<Slika> findByAutor(Autor a);
	List<Slika> findByUser(User u);
	//List <Slika> findByImeAutora(String IdAutor);
	List <Slika> findByNaziv(String naziv);
	//Slika findBySlika(String slika);
	List<Slika> findByNazivIgnoreCase(String naziv);
	
	List<Slika> findByPravac(String pravac);
	List<Slika> findByVremeNastanka(String godina);
	Optional<Slika> findByIdSlika(int najveci);

	
	
}
