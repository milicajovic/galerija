package com.example.demo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.domain.SlikaImage;
import com.example.demo.repository.AutorRepository;
import com.example.demo.repository.LikeRepository;
import com.example.demo.repository.SlikaRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UtisakRepository;
import com.example.demo.repository.ZanimljivostiRepository;

import model.Autor;
import model.Lajk;
import model.Slika;
import model.User;
import model.Utisak;




@Controller
@RequestMapping(value = "/Slike")
public class SlikaController {
	
	@Autowired
	SlikaRepository sr;
	
	@Autowired
	AutorRepository ar;
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	ZanimljivostiRepository zr;
	
	@Autowired
	UtisakRepository uur;
	
	@Autowired
	LikeRepository lr;
	
	@RequestMapping(value = "prikazSlika", method = RequestMethod.GET)
	public String prikazSlika(Model m, HttpServletRequest request) {
		List<Slika> slike = sr.findAll();
		request.getSession().setAttribute("slike", slike);
		return "PrikazSlika";

	}
	
	
	@RequestMapping(value = "/unosSlikeInit", method = RequestMethod.GET)
	public String initialize(Model m, HttpServletRequest request) {
		List<Autor> autori = ar.findAll();
		request.getSession().setAttribute("autori", autori);
		m.addAttribute("slikaIm", new SlikaImage());
		return "UnosSlike";
	}
	
	@RequestMapping(value="sacuvajSliku", method=RequestMethod.POST)
	public String sacuvajSliku(Model m, @ModelAttribute("slikaIm") SlikaImage slikaImage,HttpServletRequest request){
		MultipartFile file =slikaImage.getImage();
		if (null != file) {
			String fileName = file.getOriginalFilename();
			String filePath;
			try {
				filePath = System.getProperty("user.dir");
				System.out.println("Putanja je "+filePath);
				File imageFile = new File(filePath, fileName);

				file.transferTo(imageFile);
				String slika = request.getParameter("naziv");
				List<Slika> s = sr.findByNazivIgnoreCase(slika);
				User user=(User) request.getSession().getAttribute("user");
				int k1=user.getCounter()+1;
				System.out.println(k1);
				user.setCounter(k1);
				ur.save(user);
				if(s==null) {
					Slika s1=new Slika();
					s1.setNaziv(slikaImage.getNaziv());
					s1.setPravac(slikaImage.getPravac());
					s1.setVremeNastanka(slikaImage.getVremeNastanka());
					s1.setImage(Files.readAllBytes(imageFile.toPath()));
					s1.setUser(user);
					s1.setAutor(ar.findById(Integer.parseInt(request.getParameter("idAutor"))).get());
					sr.save(s1);
					String poruka="Uspesno uneto slika!";
					m.addAttribute("poruka", poruka);
					request.getSession().setAttribute("s1", s1);
				}
				else {
					String poruka="Neuspesno uneto slika!";
					m.addAttribute("poruka", poruka);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return "PrikazSlika";
	}


	
	@RequestMapping(value = "get-image/{id}", method = RequestMethod.GET)
	public void getImage(@PathVariable("id") int slikaId, HttpServletResponse response) {
		Slika s = sr.findById(slikaId).get();
		byte[] slikaSlika = s.getImage();
		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		try {
			response.getOutputStream().write(slikaSlika);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	
	@RequestMapping(value="/lajkuj", method=RequestMethod.POST)
	public String lajkuj(HttpServletRequest request,Model m,int idSlika,Integer idUser) {
		
		Slika slika = sr.findById(idSlika).get();
		User user=(User) request.getSession().getAttribute("user");
		Lajk vecLajkovao = lr.findByUserAndSlika(user, slika);
		
		if(vecLajkovao == null || !(vecLajkovao.getLajkovana().equals("da"))) {
			Lajk lajk = new Lajk();
			Date datum =  new Date();
			lajk.setDatum(datum.toString());
		    lajk.setSlika(slika);
			lajk.setUser(user);
			lajk.setLajkovana("da");
			lr.save(lajk);
			
		 } else if(vecLajkovao.getLajkovana().equals("da")) {
			 String poruka = "Vec ste lajkovali ovu sliku!";
			 m.addAttribute("message",poruka);
			 return "/PrikazSlika";
		 } else {
			 return "/PrikazSlika";
		 }
		
			
        
        return "/PrikazSlika";
	       
		
	}

	
	
	
	
	@RequestMapping(value = "nadjiNajveci", method = RequestMethod.GET)
	public String nadjiNajveci(Model m,HttpServletRequest request) {
		List<User> users=ur.findAll();
		
		int a=0;
		String najveci="";
		for(int i=0;i<users.size();i++) {
			if((users.get(i).getCounter()) > a) {
				a=users.get(i).getCounter();
				najveci=users.get(i).getIme();
					
			}
			
		}
		System.out.println(a);
		System.out.println(najveci);
		request.getSession().setAttribute("najveci", najveci);
		return "Proba";
		
		
	}
	
	
	@RequestMapping(value = "sveSlike", method = RequestMethod.GET)
	public String prikazSlike(Model m, HttpServletRequest request) {
		List<Slika> slike = sr.findAll();
		request.getSession().setAttribute("slike", slike);
		request.getSession().getAttribute("user");
		return "Proba";

	}
	
	@RequestMapping(value="utisci", method=RequestMethod.GET)
	public String ostaviUtisak(Integer idSlika,HttpServletRequest request) {
		Slika s=sr.findById(idSlika).get();
		request.getSession().setAttribute("s", s);
		return "UnesiUtisak";
	}
	
	
	
	@RequestMapping(value="utisak", method=RequestMethod.POST)
	public String sacuvajUtisak(HttpServletRequest request, Model m) {
		
		String komentar=request.getParameter("komentar");
		User user=(User) request.getSession().getAttribute("user");
		if(komentar!=null) {
			Utisak u= new Utisak();
			Slika s = (Slika)request.getSession().getAttribute("s");
			u.setKomentar(komentar);
			u.setUser(user);
			u.setSlika(s);
			u.setDatum(new Date());
			uur.save(u);
			return "UnesiUtisak";
		}else {
			String poruka="Obavezan unos";
			m.addAttribute("poruka",poruka);
			return "UnesiUtisak";
		}
		
		
	}
	
	@RequestMapping(value="pregledUtisaka", method=RequestMethod.GET)
	public String pregled(Integer idSlika,HttpServletRequest request) {
		Slika s=sr.findById(idSlika).get();
		request.getSession().setAttribute("s", s);
		List<Utisak> utisci=uur.findBySlika(s);
		request.getSession().setAttribute("utisci", utisci);
		Utisak u = null;
		for(int i = 0; i < utisci.size(); i++) {
			
			u = utisci.get(i);
			if(u != null) 
				break;
		}
		System.out.println(u);
		request.getSession().setAttribute("u", u);
		return "PregledUtisaka";
	}
	
	
	@RequestMapping(value = "slikePocetna", method = RequestMethod.GET)
	public String prikaz(Model m, HttpServletRequest request) {
		List<Slika> slike = sr.findAll();
		request.getSession().setAttribute("slike", slike);
		return "Pocetna";

	}
	
	public static <Lajk> Lajk mostCommon(List<Lajk>lista) {
		
		Map<Lajk,Integer> map = new HashMap();
		for(Lajk l: lista) {
		Integer val = map.get(l);
		map.put(l, val == null ? 1: val+1);
		
		}
		
		Entry<Lajk,Integer> max = null;
		for(Entry<Lajk,Integer> e:map.entrySet()) {
			if(max == null || e.getValue() > max.getValue())
				max = e;
		}
		
		return max.getKey();
		
	}
	
	
 @RequestMapping(value="najviseLajkova", method = RequestMethod.GET)
 public String slikaSaNajviseLajkova(Model m,HttpServletRequest request) {
	 
	
	 LocalDate danasnji = LocalDate.now();
	 String prosli = danasnji.minusMonths(1).toString();
	 
	 
	 List<Lajk>lajkovi  = lr.findByDatumGreaterThanEqual(prosli);
	 System.out.println(lajkovi);

	 List<Integer> lista = new ArrayList<>();
	 int a = 0;
	 for(int i = 0; i<lajkovi.size(); i++) {
		 a = lajkovi.get(i).getSlika().getIdSlika();
		 lista.add(a);
	 }
	 
	 System.out.println(lista);
	 
	 int najveci=0;
	 najveci = mostCommon(lista);
	 System.out.println(najveci);
	 
	 Optional<Slika> taSlika = sr.findByIdSlika(najveci);
	 System.out.println(taSlika.get());
	 request.getSession().setAttribute("slikaSaNajviseLajkova", taSlika.get());
	  
	 return "/Proba";
	 
 }


	

}
