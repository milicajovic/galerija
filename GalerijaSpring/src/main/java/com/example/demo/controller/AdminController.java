package com.example.demo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.domain.ZanimljivostiImage;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.ZanimljivostiRepository;

import model.Autor;
import model.Slika;
import model.User;
import model.Zanimljivosti;

@Controller
@RequestMapping(value = "/Admin")
public class AdminController {
	
	@Autowired
	ZanimljivostiRepository zr;
	

	@Autowired
	UserRepository ur;
	
	@RequestMapping(value = "/unosSlikeInit", method = RequestMethod.GET)
	public String initialize(Model m) {
		m.addAttribute("slikaIm", new ZanimljivostiImage());
		return "DodajZanimljivosti";
	}
	
	
	
	
	@RequestMapping(value="unosZanimljivosti", method=RequestMethod.POST)
	public String unosZanimljivosti(Model m, @ModelAttribute("slikaIm") ZanimljivostiImage slikaImage,HttpServletRequest request) {
		MultipartFile file =slikaImage.getSlika();
		if (null != file) {
			String fileName = file.getOriginalFilename();
			String filePath;
			try {
				filePath = System.getProperty("user.dir");
				System.out.println("Putanja je "+filePath);
				File imageFile = new File(filePath, fileName);

				file.transferTo(imageFile);
				String zanimljivost = request.getParameter("zanimljivost");
				User user=(User) request.getSession().getAttribute("user");
			
				if(zanimljivost!=null ) {
					Zanimljivosti z=new Zanimljivosti();
					z.setTekst(zanimljivost);
					z.setSlika(Files.readAllBytes(imageFile.toPath()));
					z.setUser(user);
					zr.save(z);
					return "DodajZanimljivosti";
				}else {
					String poruka="Obavezan unos";
					m.addAttribute("poruka",poruka);
					return "DodajZanimljivosti";
					
				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return "DodajZanimljivosti";
	}
	
	@RequestMapping(value = "get-image/{id}", method = RequestMethod.GET)
	public void getImage(@PathVariable("id") int idZanimljivosti, HttpServletResponse response) {
		Zanimljivosti z=zr.findById(idZanimljivosti).get();
		byte[] slikaSlika = z.getSlika();
		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		try {
			response.getOutputStream().write(slikaSlika);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value="sveZanimljivosti", method=RequestMethod.GET)
	public String pregledZanimljivosti(HttpServletRequest request) {
		List<Zanimljivosti> zanimljivosti=zr.sveZanimljivosti();
		request.getSession().setAttribute("zanimljivosti", zanimljivosti);
		return "PregledZanimljivosti";
	}
}


