package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;


import model.Role;
import model.User;

@Controller
@RequestMapping(value="/LogovanjeRegistracija")
public class RegistracijaLogovanjeController{
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	RoleRepository rr;
	
	@ModelAttribute
	public void getRoles(Model model) {
		List<Role> roles=rr.findAll();
		model.addAttribute("roles", roles);
		
	}
	
	@RequestMapping(value="pocetna", method=RequestMethod.GET) 
	public String pocetna() { 
		return "Pocetna";
	}
	
	@RequestMapping(value = "registerUser", method = RequestMethod.GET)
	public String register(Model model) {
	
		User u = new User();
		
		model.addAttribute("user", u);
		return "register";	
	}
	
	 @RequestMapping(value = "register", method = RequestMethod.POST)
		public String saveUser(@ModelAttribute("user") User u) {
	    	
	     	u.setPassword(u.getPassword());
			
			for (Role rr : u.getRoles()) {
				rr.addKorisnik(u);
				
			}
	    	ur.save(u);
			return "login";

		}	
	 
	 
	 @RequestMapping(value="login", method = RequestMethod.POST)
	 public String login(Model model,HttpServletRequest request, String username, String password) {
		
		 String poruka = "";
		 String putanja = "";
		 User u = ur.findByUsernameAndPassword(username, password);
		 if(u != null) {
			 
			putanja = "/Proba";
			poruka = "Uspjesno logovanje";
		 
		 } else {
			putanja = "/Pocetna";
			poruka = "Neuspjesno logovanje";
		 }
		
		 request.getSession().setAttribute("user", u);
		 model.addAttribute("poruka",poruka);
		 return "redirect:/Slike/sveSlike";
		 
	 }
	 
	 @RequestMapping (value="odjava", method=RequestMethod.GET)
		public String odjava (HttpServletRequest request) {
			request.getSession().removeAttribute("user");
			return "/login";
			
		}

	 
	
	    

}