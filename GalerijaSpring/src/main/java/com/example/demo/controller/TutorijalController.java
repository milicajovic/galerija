package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.ZanimljivostiRepository;

import model.Odgovor;
import model.User;
import model.Zanimljivosti;

@Controller
@RequestMapping(value="/Tutorijali")
public class TutorijalController {

	@Autowired
	ZanimljivostiRepository zr;
	
	@RequestMapping(value="unos", method=RequestMethod.POST)
	public String noviTutorijal(Model m,HttpServletRequest request) {
		
		String tutorijal=request.getParameter("tutorijal");
		User user=(User) request.getSession().getAttribute("user");
		if(tutorijal!=null) {
			Zanimljivosti z=new Zanimljivosti();
			z.setTutorijal(tutorijal);
			z.setUser(user);
			zr.save(z);
			return "UnosTutorijala";
		}else {
			String poruka="Obavezan unos";
			m.addAttribute("poruka",poruka);
			return "UnosTutorijala";
		}
			
	}
	
	@RequestMapping(value="prikazi", method=RequestMethod.GET)
	public String prikazTutorijala(HttpServletRequest request) {
		List<Zanimljivosti> tutorijali=zr.findAll();
		request.getSession().setAttribute("tutorijali", tutorijali);
		return "PregledTutorijala";
	}
	
	
}
