package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.ForumRepository;
import com.example.demo.repository.OdgovorRepository;

import model.Forum;
import model.Odgovor;
import model.User;

@Controller
@RequestMapping(value="/Forum")
public class ForumController {
	
	@Autowired
	OdgovorRepository or;
	
	@Autowired
	ForumRepository fr;

	@RequestMapping(value="sviForumi", method = RequestMethod.GET)
	public String getForumi(HttpServletRequest request) {
		List<Forum> forumi = fr.findAll();
		request.getSession().setAttribute("forumi", forumi);
		return "Odgovori";
	}
	
	@RequestMapping(value="unosForuma", method=RequestMethod.POST)
	public String unos(Model m, HttpServletRequest request) {
		
		String nazivTeme = request.getParameter("nazivTeme");
		String sadrzajTeme = request.getParameter("sadrzajTeme");
		User user=(User) request.getSession().getAttribute("user");
		if(nazivTeme != "" && sadrzajTeme != "") {
			Forum f = new Forum();
			f.setNazivTeme(nazivTeme);
			f.setSadrzajTeme(sadrzajTeme);
			f.setUser(user);
			boolean uspesno = fr.save(f) != null?true:false;
			request.getSession().setAttribute("forum", f);
			String dodat="";
			String putanja="";
			if(uspesno){
				dodat = "Uspesno dodat forum";
				putanja="/SviForumi";
			}else {
				dodat = "Neuspesno dodavanje";
				putanja = "/SviForumi";
			}
			m.addAttribute("dodat", dodat);
			return putanja;
		}else {
			String prazanNaziv="Unesite naziv teme.";
			m.addAttribute("prazanNaziv", prazanNaziv);
			return "/SviForumi";
			
			//treba napraviti jsp stranicu na kojoj ce biti svi forumi prikazani, zove se bas kao u return SviForumi
		}
	}
	
	@RequestMapping(value="dodajOdgovor", method=RequestMethod.POST)
	public String dodajOdg(HttpServletRequest request, Model m) {
		
		Odgovor o = new Odgovor();
		String tekstOdgovora = request.getParameter("tekstOdgovora");
		User user =  (User) request.getSession().getAttribute("user");
		o.setTekstOdgovora(tekstOdgovora);
		o.setIdUser(user);
		
		o.setIdForum(fr.findById(Integer.parseInt(request.getParameter("trenutniForum"))).get());
		if(tekstOdgovora != "") {
			if(or.save(o) != null) {
				String poruka = "Uspesno dodat odgovor na forum ";
				m.addAttribute("poruka", poruka);
			}
		}else {
			String nepotpuno="Unesite odgovor";
			m.addAttribute("nepotpuno", nepotpuno);
		}
		return "Odgovori";	
		
		//treba napraviti jsp stranicu Odgovori na kojoj ce biti svi odgovori za izabrani forum, 
		//kada se doda odgovor, ode se bas na tu jsp Odgovori gde su svi vec postojeci, pa i taj novi
	}
	

	@RequestMapping(value="forumi", method = RequestMethod.GET)
	public String sviForumi(HttpServletRequest request) {
		List<Forum> forumi = fr.findAll();
		request.getSession().setAttribute("forumi", forumi);
		return "PregledOdgovora";
	}
	
	@RequestMapping(value="getOdgovoriZaForum", method=RequestMethod.GET)
	public String getOdg(Integer idForum, Model m) {
		Forum f = fr.findById(idForum).get();
		List<Odgovor> odgovori = or.findByForum(f);
		m.addAttribute("odgovori", odgovori);
		return "PregledOdgovora";
		
		
	}
	
	@RequestMapping(value="koJeNapisao", method=RequestMethod.GET)
	public String getOdgKorisnik(int idOdgovor, Model m) {
		Odgovor o = or.findById(idOdgovor).get();
		User u = o.getUser();
		String ime = u.getIme();
		String poruka3 = "Odgovor je napisao korisnik " + ime;
		m.addAttribute("poruka3", poruka3);
		return "PregledOdgovora";
	}
	
	@RequestMapping(value="pregledForuma", method = RequestMethod.GET)
	public String pregled(HttpServletRequest request) {
		List<Forum> forumi = fr.findAll();
		request.getSession().setAttribute("forumi", forumi);
		return "SviForumi";
	}
}
