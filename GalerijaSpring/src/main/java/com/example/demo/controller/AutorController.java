package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.AutorRepository;
import com.example.demo.repository.Slika3Repository;
import com.example.demo.repository.SlikaRepository;
import com.example.demo.repository.UserRepository;

import model.Autor;

import model.Slika;
import model.User;





@Controller
@RequestMapping(value="/Autor")
public class AutorController {
	
	@Autowired
	AutorRepository ar;
	
	@Autowired
	SlikaRepository sr;
	
	@Autowired
	UserRepository ur;
	
	@Autowired
	Slika3Repository s3;

	
	@RequestMapping(value="getAutora", method=RequestMethod.GET)
	public String getAutora(int idAutor, Model m) {
		Autor aut = ar.findById(idAutor).get();
		List<Slika> slike = sr.findByAutor(aut);
		m.addAttribute("slike", slike);
		return "PrikazSlika";
	}
	
	@RequestMapping(value="getAutore", method=RequestMethod.GET)
	public String getAutore(HttpServletRequest request) {
		List<Autor> autori = ar.findAll();
		request.getSession().setAttribute("autori", autori);
		return "UnosAutora";
	}
	
	
	public static <T> T mostCommon(List<T> lista) {
	    Map<T, Integer> map = new HashMap<>();

	    for (T t : lista) {
	        Integer val = map.get(t);
	        map.put(t, val == null ? 1 : val + 1);
	    }

	    Entry<T, Integer> max = null;

	    for (Entry<T, Integer> e : map.entrySet()) {
	        if (max == null || e.getValue() > max.getValue())
	            max = e;
	    }

	    return max.getKey();
	}

	
	
	@RequestMapping(value="prikazAutora", method=RequestMethod.GET)
	public String biografijaAutora(Integer idAutor,Integer idSlika,HttpServletRequest request, Model m) {
		Autor autor=ar.findById(idAutor).get();
		System.out.println(autor);
		request.getSession().setAttribute("autor", autor);
		List<Slika> slike=sr.findByAutor(autor);
		
		List<Integer> lista= new ArrayList<>();
		int a = 0;
		for(int i = 0; i < slike.size(); i++) {
			a = slike.get(i).getUser().getIdUser();
			lista.add(a);
		}
		System.out.println(lista);
		
		System.out.println(mostCommon(lista));
		int najveci = 0;
		najveci = mostCommon(lista);
		System.out.println(najveci);
		int jedan = 0;
		jedan = 6;
		Optional<Slika> taSlika = s3.findByIdSlika(jedan);
		System.out.println(taSlika.get());
		Slika bio = null;
		System.out.println(bio);
		for(int i = 0; i < slike.size(); i++) {
			
			bio = slike.get(i);
			if(bio != null) 
				break;
		}
		System.out.println(bio);
		request.getSession().setAttribute("bio", bio);
		return "Biografija";
	}
	
	


	
	
	@RequestMapping(value="getSlike", method=RequestMethod.GET)
	public String getSlike(int idAutor, Model m) {
		Autor aut = ar.findById(idAutor).get();
		List<Slika> slike = sr.findByAutor(aut);
		m.addAttribute("slike", slike);
		return "PrikazSlika";
	}
	
	@RequestMapping(value="unosAutora", method=RequestMethod.POST)
	public String unosAutora(HttpServletRequest request, Model m) {
		
		String imeAutora = request.getParameter("imeAutora");
		String biografija= request.getParameter("biografija");
		Autor a = ar.findByImeAutora(imeAutora);
		if (a==null) {
			Autor noviAutor = new Autor();
			noviAutor.setImeAutora(imeAutora);
			noviAutor.setBiografija(biografija);
			ar.save(noviAutor);
			m.addAttribute("noviAutorPoruka", "UspeĹˇno dodavanje novog autora.");
			//return "redirect:/Slika/sacuvajSliku";
			return "redirect:/Slike/unosSlikeInit";
		} else {
			m.addAttribute("noviAutorPoruka", "Autor kojeg pokuĹˇavate da dodate veÄ‡ postoji!");
			return "/UnosAutora.jsp";
		}	
	}
	
	@RequestMapping(value="pretragaSlikari", method=RequestMethod.GET)
	public String sviSlikari(Model m) {
		List<Autor> autori=ar.findAll();
		m.addAttribute("autori",autori);
		return "PretragaSlikari";
	}
	
	
	@RequestMapping(value="getPretragaSlikari", method=RequestMethod.GET) 
	public String getPretragaSlikari(HttpServletRequest request, Integer idAutor) { 
		Autor a = ar.findById(idAutor).get();
		List<Slika> slike = sr.findByAutor(a);
		request.getSession().setAttribute("slike", slike);
		return "PretragaSlikari";
	}
	
	
	
	@RequestMapping(value="getPretragaNazivi", method=RequestMethod.GET) 
	public String getPretragaNazivi(HttpServletRequest request) { 
		String naziv = request.getParameter("naziv").toLowerCase();
		request.getSession().setAttribute("naziv", naziv);
		List<Slika> slike = sr.findByNaziv(naziv);
		request.getSession().setAttribute("slike", slike);
		return "PretragaNaziv";
	}

	
	
	@RequestMapping(value="getPretragaPravci", method=RequestMethod.GET) 
	public String getPretragaPravci(HttpServletRequest request) { 
		String pravac = request.getParameter("pravac").toLowerCase();
		request.getSession().setAttribute("pravac", pravac);
		List<Slika> slike = sr.findByPravac(pravac);
		
		request.getSession().setAttribute("slike", slike);
		return "PretragaPravac";
	}
	
	
	@RequestMapping(value="getPretragaGodina", method=RequestMethod.GET) 
	public String getPretragaGodina(HttpServletRequest request) { 
		String godina = request.getParameter("godina");
		request.getSession().setAttribute("godina", godina);
		List<Slika> slike = sr.findByVremeNastanka(godina);
		request.getSession().setAttribute("slike", slike);
		return "PretragaGodine";
	}
	
	
	@RequestMapping(value="pretragaKorisnik", method=RequestMethod.GET)
	public String sviKorisnici(Model m) {
		List<User> user=ur.sviKorisnici();
		m.addAttribute("user",user);
		return "PretragaKorisnici";
	}
	
	

	@RequestMapping(value="getPretragaKorisnik", method=RequestMethod.GET) 
	public String getPretragaKorisnici(HttpServletRequest request, Integer idUser,Model m) { 
		request.getSession().removeAttribute("slike");
		System.out.println(idUser);
		request.getSession().removeAttribute("user");
		System.out.println(idUser);
		User u = ur.findById(idUser).get();
		System.out.println(u);
		List<Slika> slike = sr.findByUser(u);
		System.out.println(slike);
		
		String poruka="prazno";
		System.out.println(poruka);
		if(slike.isEmpty()) {
			poruka="Odabrani korisnik nije dodao nijednu sliku";
			request.getSession().setAttribute("poruka", poruka);
			System.out.println(poruka);
		}
		else {
			poruka="Korisnik je dodavao sledece slike";
			request.getSession().setAttribute("slike", slike);
			System.out.println(slike);
			System.out.println(poruka);
			
		}
		return "PretragaKorisnici";
	}

	
	
	


}
