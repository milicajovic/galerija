package com.example.demo.domain;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class SlikaImage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int idSlika;
	private String naziv;
	private String pravac;
	private String vremeNastanka;
	private int idUser;
	private int idAutor;
	private MultipartFile image;
	public int getIdSlika() {
		return idSlika;
	}
	public void setIdSlika(int idSlika) {
		this.idSlika = idSlika;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getPravac() {
		return pravac;
	}
	public void setPravac(String pravac) {
		this.pravac = pravac;
	}
	public String getVremeNastanka() {
		return vremeNastanka;
	}
	public void setVremeNastanka(String vremeNastanka) {
		this.vremeNastanka = vremeNastanka;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdAutor() {
		return idAutor;
	}
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
