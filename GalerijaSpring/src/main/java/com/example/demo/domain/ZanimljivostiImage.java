package com.example.demo.domain;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class ZanimljivostiImage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int idZanimljivosti;
	private String tekst;
	private int idUser;
	private MultipartFile slika;
	
	
	public int getIdZanimljivosti() {
		return idZanimljivosti;
	}
	public void setIdZanimljivosti(int idZanimljivosti) {
		this.idZanimljivosti = idZanimljivosti;
	}
	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	
	public MultipartFile getSlika() {
		return slika;
	}
	public void setSlika(MultipartFile slika) {
		this.slika = slika;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
