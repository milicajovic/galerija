package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the utisak database table.
 * 
 */
@Entity
@NamedQuery(name="Utisak.findAll", query="SELECT u FROM Utisak u")
public class Utisak implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUtisak;

	private String komentar;

	//bi-directional many-to-one association to Slika
	@ManyToOne
	@JoinColumn(name="idSlika")
	private Slika slika;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser")
	private User user;

	public Utisak() {
	}

	public int getIdUtisak() {
		return this.idUtisak;
	}

	public void setIdUtisak(int idUtisak) {
		this.idUtisak = idUtisak;
	}

	public String getKomentar() {
		return this.komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public Slika getSlika() {
		return this.slika;
	}

	public void setSlika(Slika slika) {
		this.slika = slika;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}