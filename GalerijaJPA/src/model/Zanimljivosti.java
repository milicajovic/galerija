package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the zanimljivosti database table.
 * 
 */
@Entity
@NamedQuery(name="Zanimljivosti.findAll", query="SELECT z FROM Zanimljivosti z")
public class Zanimljivosti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idZanimljivosti;

	@Lob
	private byte[] slika;

	private String tekst;

	private String tutorijal;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser")
	private User user;

	public Zanimljivosti() {
	}

	public int getIdZanimljivosti() {
		return this.idZanimljivosti;
	}

	public void setIdZanimljivosti(int idZanimljivosti) {
		this.idZanimljivosti = idZanimljivosti;
	}

	public byte[] getSlika() {
		return this.slika;
	}

	public void setSlika(byte[] slika) {
		this.slika = slika;
	}

	public String getTekst() {
		return this.tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public String getTutorijal() {
		return this.tutorijal;
	}

	public void setTutorijal(String tutorijal) {
		this.tutorijal = tutorijal;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}