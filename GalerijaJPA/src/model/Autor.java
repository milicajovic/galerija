package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the autor database table.
 * 
 */
@Entity
@NamedQuery(name="Autor.findAll", query="SELECT a FROM Autor a")
public class Autor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAutor;

	private String biografija;

	private String imeAutora;

	//bi-directional many-to-one association to Slika
	@ManyToOne
	@JoinColumn(name="idSlika")
	private Slika slika;

	//bi-directional many-to-one association to Slika
	@OneToMany(mappedBy="autor")
	private List<Slika> slikas;

	public Autor() {
	}

	public int getIdAutor() {
		return this.idAutor;
	}

	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}

	public String getBiografija() {
		return this.biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public String getImeAutora() {
		return this.imeAutora;
	}

	public void setImeAutora(String imeAutora) {
		this.imeAutora = imeAutora;
	}

	public Slika getSlika() {
		return this.slika;
	}

	public void setSlika(Slika slika) {
		this.slika = slika;
	}

	public List<Slika> getSlikas() {
		return this.slikas;
	}

	public void setSlikas(List<Slika> slikas) {
		this.slikas = slikas;
	}

	public Slika addSlika(Slika slika) {
		getSlikas().add(slika);
		slika.setAutor(this);

		return slika;
	}

	public Slika removeSlika(Slika slika) {
		getSlikas().remove(slika);
		slika.setAutor(null);

		return slika;
	}

}