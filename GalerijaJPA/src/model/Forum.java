package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the forum database table.
 * 
 */
@Entity
@NamedQuery(name="Forum.findAll", query="SELECT f FROM Forum f")
public class Forum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idForum;

	private String nazivTeme;

	private String sadrzajTeme;

	//bi-directional many-to-one association to Slika
	@ManyToOne
	@JoinColumn(name="idSlika")
	private Slika slika;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser")
	private User user;

	public Forum() {
	}

	public int getIdForum() {
		return this.idForum;
	}

	public void setIdForum(int idForum) {
		this.idForum = idForum;
	}

	public String getNazivTeme() {
		return this.nazivTeme;
	}

	public void setNazivTeme(String nazivTeme) {
		this.nazivTeme = nazivTeme;
	}

	public String getSadrzajTeme() {
		return this.sadrzajTeme;
	}

	public void setSadrzajTeme(String sadrzajTeme) {
		this.sadrzajTeme = sadrzajTeme;
	}

	public Slika getSlika() {
		return this.slika;
	}

	public void setSlika(Slika slika) {
		this.slika = slika;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}