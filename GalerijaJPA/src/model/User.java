package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUser;

	private String ime;

	private String password;

	private String prezime;

	private String username;

	//bi-directional many-to-one association to Forum
	@OneToMany(mappedBy="user")
	private List<Forum> forums;

	//bi-directional many-to-many association to Role
	@ManyToMany(mappedBy="users")
	private List<Role> roles;

	//bi-directional many-to-one association to Slika
	@OneToMany(mappedBy="user")
	private List<Slika> slikas;

	//bi-directional many-to-one association to Utisak
	@OneToMany(mappedBy="user")
	private List<Utisak> utisaks;

	//bi-directional many-to-one association to Zanimljivosti
	@OneToMany(mappedBy="user")
	private List<Zanimljivosti> zanimljivostis;

	public User() {
	}

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getIme() {
		return this.ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrezime() {
		return this.prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Forum> getForums() {
		return this.forums;
	}

	public void setForums(List<Forum> forums) {
		this.forums = forums;
	}

	public Forum addForum(Forum forum) {
		getForums().add(forum);
		forum.setUser(this);

		return forum;
	}

	public Forum removeForum(Forum forum) {
		getForums().remove(forum);
		forum.setUser(null);

		return forum;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Slika> getSlikas() {
		return this.slikas;
	}

	public void setSlikas(List<Slika> slikas) {
		this.slikas = slikas;
	}

	public Slika addSlika(Slika slika) {
		getSlikas().add(slika);
		slika.setUser(this);

		return slika;
	}

	public Slika removeSlika(Slika slika) {
		getSlikas().remove(slika);
		slika.setUser(null);

		return slika;
	}

	public List<Utisak> getUtisaks() {
		return this.utisaks;
	}

	public void setUtisaks(List<Utisak> utisaks) {
		this.utisaks = utisaks;
	}

	public Utisak addUtisak(Utisak utisak) {
		getUtisaks().add(utisak);
		utisak.setUser(this);

		return utisak;
	}

	public Utisak removeUtisak(Utisak utisak) {
		getUtisaks().remove(utisak);
		utisak.setUser(null);

		return utisak;
	}

	public List<Zanimljivosti> getZanimljivostis() {
		return this.zanimljivostis;
	}

	public void setZanimljivostis(List<Zanimljivosti> zanimljivostis) {
		this.zanimljivostis = zanimljivostis;
	}

	public Zanimljivosti addZanimljivosti(Zanimljivosti zanimljivosti) {
		getZanimljivostis().add(zanimljivosti);
		zanimljivosti.setUser(this);

		return zanimljivosti;
	}

	public Zanimljivosti removeZanimljivosti(Zanimljivosti zanimljivosti) {
		getZanimljivostis().remove(zanimljivosti);
		zanimljivosti.setUser(null);

		return zanimljivosti;
	}

}