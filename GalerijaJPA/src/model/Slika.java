package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the slika database table.
 * 
 */
@Entity
@NamedQuery(name="Slika.findAll", query="SELECT s FROM Slika s")
public class Slika implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idSlika;

	@Lob
	private byte[] image;

	private String naziv;

	private String pravac;

	private String vremeNastanka;

	//bi-directional many-to-one association to Autor
	@OneToMany(mappedBy="slika")
	private List<Autor> autors;

	//bi-directional many-to-one association to Forum
	@OneToMany(mappedBy="slika")
	private List<Forum> forums;

	//bi-directional many-to-one association to Autor
	@ManyToOne
	@JoinColumn(name="idAutor")
	private Autor autor;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="idUser")
	private User user;

	//bi-directional many-to-one association to Utisak
	@OneToMany(mappedBy="slika")
	private List<Utisak> utisaks;

	public Slika() {
	}

	public int getIdSlika() {
		return this.idSlika;
	}

	public void setIdSlika(int idSlika) {
		this.idSlika = idSlika;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPravac() {
		return this.pravac;
	}

	public void setPravac(String pravac) {
		this.pravac = pravac;
	}

	public String getVremeNastanka() {
		return this.vremeNastanka;
	}

	public void setVremeNastanka(String vremeNastanka) {
		this.vremeNastanka = vremeNastanka;
	}

	public List<Autor> getAutors() {
		return this.autors;
	}

	public void setAutors(List<Autor> autors) {
		this.autors = autors;
	}

	public Autor addAutor(Autor autor) {
		getAutors().add(autor);
		autor.setSlika(this);

		return autor;
	}

	public Autor removeAutor(Autor autor) {
		getAutors().remove(autor);
		autor.setSlika(null);

		return autor;
	}

	public List<Forum> getForums() {
		return this.forums;
	}

	public void setForums(List<Forum> forums) {
		this.forums = forums;
	}

	public Forum addForum(Forum forum) {
		getForums().add(forum);
		forum.setSlika(this);

		return forum;
	}

	public Forum removeForum(Forum forum) {
		getForums().remove(forum);
		forum.setSlika(null);

		return forum;
	}

	public Autor getAutor() {
		return this.autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Utisak> getUtisaks() {
		return this.utisaks;
	}

	public void setUtisaks(List<Utisak> utisaks) {
		this.utisaks = utisaks;
	}

	public Utisak addUtisak(Utisak utisak) {
		getUtisaks().add(utisak);
		utisak.setSlika(this);

		return utisak;
	}

	public Utisak removeUtisak(Utisak utisak) {
		getUtisaks().remove(utisak);
		utisak.setSlika(null);

		return utisak;
	}

}